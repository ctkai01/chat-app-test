import React, { useEffect, useState, useRef } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { io } from "socket.io-client";
import styled from "styled-components";
import { allUsersRoute, host } from "../utils/APIRoutes";
import ChatContainer from "../components/ChatContainer";
import Contacts from "../components/Contacts";
import Welcome from "../components/Welcome";

export default function Chat() {
  const navigate = useNavigate();
  const socket = useRef();
  const [contacts, setContacts] = useState([]);
  const [channelRoomText, setChannelRoomText] = useState("");
  const [currentChat, setCurrentChat] = useState(undefined);
  const [currentUser, setCurrentUser] = useState(undefined);
  useEffect(async () => {
    if (!localStorage.getItem(process.env.REACT_APP_LOCALHOST_KEY)) {
      navigate("/login");
    } else {
      setCurrentUser(
        await JSON.parse(
          localStorage.getItem(process.env.REACT_APP_LOCALHOST_KEY)
        )
      );
    }
  }, []);
  useEffect(() => {
    const connectSocket = () => {
      if (currentUser) {
        socket.current = io("http://localhost:3000");
        socket.current.connect();
        socket.current.emit("add-user", currentUser.id);

        console.log("Socket: ", socket);
      }
    };
    connectSocket();
    return () => {
      if (socket.current) {
        socket.current.disconnect();
      }
    };
  }, [currentUser]);

  useEffect(async () => {
    if (currentUser) {
      // if (currentUser.isAvatarImageSet) {
      const customHeaders = {
        Authorization: `Bearer ${localStorage.getItem("token")}`, // Add any other headers you need
      };
      const options = {
        headers: customHeaders,
      };
      const data = await axios.get(
        `http://localhost:3000/api/channels`,
        options
      );
      setContacts(data.data.data);
      // } else {
      //   navigate("/setAvatar");
      // }
    }
  }, [currentUser]);
  const handleChatChange = (chat) => {
    setCurrentChat(chat);
  };

  const handleCreateChannel = async () => {
    // channelRoomText;
    try {
      const formData = new FormData();
      const customHeaders = {
        Authorization: `Bearer ${localStorage.getItem("token")}`, // Add any other headers you need
      };
      const options = {
        headers: customHeaders,
      };
      // Append the key-value pair to the FormData object
      formData.append("receiveId", +channelRoomText);
      const data = await axios.post(
        `http://localhost:3000/api/channels`,

        formData,

        options
      );

      console.log("Data response: ", data);
    } catch (e) {
      console.log("Err:", e);
    }
  };
  return (
    <>
      <Container>
        <div>
          <input
            type="text"
            onChange={(e) => {
              setChannelRoomText(e.target.value);
            }}
            value={channelRoomText}
          ></input>
          <button onClick={handleCreateChannel}>Create room</button>
        </div>

        <div className="container">
          <Contacts contacts={contacts} changeChat={handleChatChange} />
          {currentChat === undefined ? (
            <Welcome />
          ) : (
            <>
              <ChatContainer currentChat={currentChat} socket={socket} />
            </>
          )}
        </div>
      </Container>
    </>
  );
}

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 1rem;
  align-items: center;
  background-color: #131324;
  .container {
    height: 85vh;
    width: 85vw;
    background-color: #00000076;
    display: grid;
    grid-template-columns: 25% 75%;
    @media screen and (min-width: 720px) and (max-width: 1080px) {
      grid-template-columns: 35% 65%;
    }
  }
`;
