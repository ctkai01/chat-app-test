import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import ChatInput from "./ChatInput";
import Logout from "./Logout";
import { v4 as uuidv4 } from "uuid";
import axios from "axios";
import { sendMessageRoute, recieveMessageRoute } from "../utils/APIRoutes";

export default function ChatContainer({ currentChat, socket }) {
  const [messages, setMessages] = useState([]);
  const [statusMessage, setStatusMessage] = useState({
    current: 1,
    total: null,
  });
  const scrollRef = useRef();
  const scrollContainerRef = useRef();
  const [loading, setLoading] = useState(false);
  const [arrivalMessage, setArrivalMessage] = useState(null);

  useEffect(async () => {
    setLoading(true);

    const data = await JSON.parse(
      localStorage.getItem(process.env.REACT_APP_LOCALHOST_KEY)
    );
    const customHeaders = {
      Authorization: `Bearer ${localStorage.getItem("token")}`, // Add any other headers you need
    };
    const options = {
      headers: customHeaders,
    };
    const response = await axios.get(
      `http://localhost:3000/api/messages/?channel_id=${currentChat._id}&page=1`,
      options
    );

    const dataMessages = response.data.data.map((message) => {
       console.log("Fet 1: ", message);
      if (message.message.user.ID == data.id) {
        return {
          message: message.message.text,
          user: message.message.user,
          fromSelf: true,
          time: message.create_at,
        };
      } else {
        return {
          message: message.message.text,
          fromSelf: false,
          user: message.message.user,

          time: message.create_at,
        };
      }
    });
    console.log("dataMessages: ", dataMessages);
    setStatusMessage({
      ...statusMessage,
      total: response.data.page.totalPage,
    });
    setMessages(dataMessages);
    setLoading(false);
  }, [currentChat]);

  const loadOlderMessages = async () => {
    if (loading) {
      return;
    }
    // console.log("Call API.....");

    setLoading(true);
    const customHeaders = {
      Authorization: `Bearer ${localStorage.getItem("token")}`, // Add any other headers you need
    };
    const options = {
      headers: customHeaders,
    };
    console.log("Call API.....");

    const response = await axios.get(
      `http://localhost:3000/api/messages/?channel_id=${currentChat._id}&page=${
        statusMessage.current + 1
      }`,
      options
    );
    const data = await JSON.parse(
      localStorage.getItem(process.env.REACT_APP_LOCALHOST_KEY)
    );
    const dataMessages = response.data.data.map((message) => {
      console.log("Fet: ", message);
      if (message.message.user.ID == data.id) {
        return {
          message: message.message.text,
          fromSelf: true,
          time: message.create_at,
        };
      } else {
        return {
          message: message.message.text,
          fromSelf: false,
          time: message.create_at,
        };
      }
    });
    setStatusMessage({
      current: response.data.page.page,
      total: response.data.page.totalPage,
    });
    setMessages((prevMessage) => [...dataMessages, ...prevMessage]);
    // Simulate loading with a delay (remove in production)
    setLoading(false);

    // setTimeout(() => {
    //   setLoading(false);
    // }, 1000);
  };

  const handleSendMsg = async (msg) => {
    const data = await JSON.parse(
      localStorage.getItem(process.env.REACT_APP_LOCALHOST_KEY)
    );
    // console.log("What: ", {
    //   to: currentChat.contact.userId,
    //   from: data.id,
    //   msg,
    // });
    // console.log("currentChat", currentChat);
    console.log("currentChat.contact:", currentChat.contact);
    socket.current.emit("send-msg", {
      to: currentChat.contact.ID,
      from: data.id,
      msg,
    });
    const formData = new FormData();
    const customHeaders = {
      Authorization: `Bearer ${localStorage.getItem("token")}`, // Add any other headers you need
    };
    const options = {
      headers: customHeaders,
    };
    formData.append("channelId", currentChat._id);
    formData.append("message", msg);

    await axios.post(
      `http://localhost:3000/api/messages`,
      formData,

      options
    );

    const msgs = [...messages];
    const now = new Date();
    const nowDate = now.toISOString();
    msgs.push({ fromSelf: true, message: msg, time: nowDate });
    setMessages(msgs);
  };

  useEffect(() => {
    if (socket.current) {
      socket.current.on("msg-recieve", (msg) => {
        const now = new Date();
        const nowDate = now.toISOString();
        setArrivalMessage({ fromSelf: false, message: msg, time: nowDate });
      });
    }
  }, []);

  useEffect(() => {
    arrivalMessage && setMessages((prev) => [...prev, arrivalMessage]);
  }, [arrivalMessage]);

  useEffect(() => {
    scrollRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [messages]);
  // console.log("scrollRef: ", scrollRef);

  useEffect(() => {
    const handleScroll = () => {
      console.log("Event scroll: ", statusMessage);
      if (scrollContainerRef.current) {
        const { scrollTop, clientHeight, scrollHeight } =
          scrollContainerRef.current;
        if (
          scrollTop === 0 &&
          !loading &&
          statusMessage.current < statusMessage.total
        ) {
          // Scrolled to the top; load older messages
          loadOlderMessages();
        }
      }
    };
    if (scrollContainerRef.current) {
      console.log("What: ", scrollContainerRef.current);
      scrollContainerRef.current.addEventListener("scroll", handleScroll);

      return () => {
        // Clean up the event listener when the component unmounts
        scrollContainerRef.current.removeEventListener("scroll", handleScroll);
      };
    }
  }, [loading]);
  return (
    <Container>
      <div className="chat-header">
        <div className="user-details">
          <div className="avatar">
            {/* <img
              src={`data:image/svg+xml;base64,${currentChat.avatarImage}`}
              alt=""
            /> */}
          </div>
          <div className="username">
            <h3>{currentChat.username}</h3>
          </div>
        </div>
        <Logout />
      </div>
      <div ref={scrollContainerRef} className="chat-messages">
        {loading && <Loading />}
        {/* <Loading /> */}
        {messages.map((message) => {
          return (
            <div ref={scrollRef} key={uuidv4()}>
              <div
                className={`message ${
                  message.fromSelf ? "sended" : "recieved"
                }`}
              >
                <div
                  style={{
                    display: "flex",
                  }}
                  className="content "
                >
                  {/* <div>
                    <img width={30} src={message.user.Avatar} />
                  </div> */}
                  <p>{message.message}</p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <ChatInput handleSendMsg={handleSendMsg} />
    </Container>
  );
}

const Loading = () => {
  return (
    <div style={{ color: "#fff", textAlign: "center" }} className="loading">
      <p>Loading...</p>
    </div>
  );
};

const Container = styled.div`
  display: grid;
  grid-template-rows: 10% 80% 10%;
  gap: 0.1rem;
  overflow: hidden;
  @media screen and (min-width: 720px) and (max-width: 1080px) {
    grid-template-rows: 15% 70% 15%;
  }
  .chat-header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 2rem;
    .user-details {
      display: flex;
      align-items: center;
      gap: 1rem;
      .avatar {
        img {
          height: 3rem;
        }
      }
      .username {
        h3 {
          color: white;
        }
      }
    }
  }
  .chat-messages {
    padding: 1rem 2rem;
    display: flex;
    flex-direction: column;
    gap: 1rem;
    overflow: auto;
    &::-webkit-scrollbar {
      width: 0.2rem;
      &-thumb {
        background-color: #ffffff39;
        width: 0.1rem;
        border-radius: 1rem;
      }
    }
    .message {
      display: flex;
      align-items: center;
      .content {
        max-width: 40%;
        overflow-wrap: break-word;
        padding: 1rem;
        font-size: 1.1rem;
        border-radius: 1rem;
        color: #d1d1d1;
        @media screen and (min-width: 720px) and (max-width: 1080px) {
          max-width: 70%;
        }
      }
    }
    .sended {
      justify-content: flex-end;
      .content {
        background-color: #4f04ff21;
      }
    }
    .recieved {
      justify-content: flex-start;
      .content {
        background-color: #9900ff20;
      }
    }
  }
`;
