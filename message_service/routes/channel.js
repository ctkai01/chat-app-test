const {
  createChannel,
  getAllChannel,
  // getChannel,
} = require("../controllers/channelController");
const router = require("express").Router();

router.post("/", createChannel);
router.get("/", getAllChannel);
// router.get("/:id", getChannel);

module.exports = router;
