const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
// const authRoutes = require("./routes/auth");
const messageRoutes = require("./routes/messages");
const channelRoutes = require("./routes/channel");
const app = express();
const socket = require("socket.io");
const bodyParser = require("body-parser");
const passport = require("./middlewares/auth-middleware");
const multer = require("multer");
const userGrpc = require("./service/user-grpc-service");

const upload = multer();
require("dotenv").config();
// app.use((req, res, next) => {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept"
//   );
//   next();
// });
app.use(
  cors({
    origin: ["http://localhost:3001", "http://localhost:5173"],
    methods: ["GET", "PUT", "POST", "DELETE"],
    credentials: true,
  })
);

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded());
app.use(upload.array());
// in latest body-parser use like below.
// app.use(bodyParser.urlencoded({ extended: true }));

// app.use(
//   bodyParser.urlencoded({
//     extended: true,
//   })
// );

const initConnect = async () => {
  try {
    mongoose.connect(process.env.MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log("DB Connetion Successfull");
  } catch (err) {
    console.log(err.message);
  }
};
initConnect()
app.use(passport.initialize());

// app.use("/api/auth", authRoutes);
// app.use("/api/messages", messageRoutes);
global.userClientConn = userGrpc.userClientConn();
app.use(
  "/api/channels",
  passport.authenticate("jwt", { session: false }),
  channelRoutes
);
app.use(
  "/api/messages",
  passport.authenticate("jwt", { session: false }),
  messageRoutes
);
// app.get("/api/test", function(req, res) {
//   return res.json("Hello world")
// })
const server = app.listen(process.env.PORT, () =>
  console.log(`Server started on ${process.env.PORT}`)
);
const io = socket(server, {
  cors: {
    // origin: ["https://kaidemy.click"],
    origin: ["http://localhost:3001", "http://localhost:5173"],
    // origin: "https://kaidemy.click",
    credentials: true,
  },
});

global.onlineUsers = new Map();
io.on("connection", (socket) => {
  // console.log("Connection: ", socket)
  global.chatSocket = socket;
  socket.on("add-user", (userId) => {
    console.log("SocketID: ", socket.id);
    console.log("userId: ", userId);

    onlineUsers.set(userId, socket.id);
    // clientRedis.hset("onlineUsers", userId, socket.id);
    console.log("onlineUsers: ", onlineUsers);
  });

  socket.on("send-msg", (data) => {
    console.log("onlineUsers: ", onlineUsers);
    const sendUserSocket = onlineUsers.get(data.to);
    console.log("Data: ", data);
    console.log("sendUserSocket: ", sendUserSocket);
    if (sendUserSocket) {
      socket.to(sendUserSocket).emit("msg-receive", data.msg);
    }
  });
});


