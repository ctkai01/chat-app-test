const roleUser = {
  SUPPER_ADMIN: 0,
  ADMIN: 1,
  NORMAL_USER: 2,
  TEACHER: 3,
};


module.exports = roleUser;