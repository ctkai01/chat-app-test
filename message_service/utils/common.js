module.exports.calculatePageSize = (page, size) => {
  const newPage = parseInt(page) || 1;

  const pageSize = size || 10;

  
  const skip = (newPage - 1) * pageSize;

  return {
    skip, pageSize
  }
};
